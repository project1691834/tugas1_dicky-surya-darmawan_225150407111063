package com.project.tugas1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView txtNamaDanNIM;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtNamaDanNIM = (TextView) findViewById(R.id.txtNamaDanNIM);
        txtNamaDanNIM.setText("Nama : Dicky Surya Darmawan" + "\n" + "NIM : 225150407111063");
    }
}